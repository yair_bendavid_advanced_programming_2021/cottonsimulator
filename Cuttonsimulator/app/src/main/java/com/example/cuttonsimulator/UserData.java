package com.example.cuttonsimulator;

import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.database.Exclude;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class UserData
{
    private int Cutton;
    private int Level;
    private List<Integer> skins;
    private List<Integer> characters;
    private List<Integer> body;
    private int EventCoins;
    private equip Equip;
    private equip EquipC;
    private equip EquipB;
    private int bestScore;
    private upgradesManager upgradesManager;

    public UserData()
    {}

    public UserData(int Cutton, int Level, int EventCoins, List<Integer> skin, equip e, int bestScore, List<Integer> character, equip eC, upgradesManager UM, List<Integer> body, equip eB)
    {
        this.Cutton = Cutton;
        this.Level = Level;
        this.skins = skin;
        this.EventCoins = EventCoins;
        this.Equip = e;
        this.bestScore = bestScore;
        this.characters = character;
        this.EquipC = eC;
        this.upgradesManager = UM;
        this.body = body;
        this.EquipB = eB;
    }

    public equip getEquipB() {
        return EquipB;
    }

    public List<Integer> getBody() {
        return body;
    }

    public void setBody(List<Integer> body) {
        this.body = body;
    }

    public void setEquipB(equip equipB) {
        EquipB = equipB;
    }

    public equip getEquipC() {
        return EquipC;
    }

    public upgradesManager getUpgradesManager() {
        return upgradesManager;
    }

    public void setUpgradesManager(upgradesManager UM) {
        this.upgradesManager = UM;
    }

    public List<Integer> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Integer> characters) {
        this.characters = characters;
    }

    public void setEquipC(equip equipC) {
        EquipC = equipC;
    }

    public int getCutton() {
        return Cutton;
    }

    public int getBestScore() {
        return bestScore;
    }

    public equip getEquip() {
        return Equip;
    }

    public int getEventCoins() {
        return EventCoins;
    }

    public int getLevel() {
        return Level;
    }

    public List<Integer> getSkins() {
        return skins;
    }


    public void setCutton(int cutton) {
        Cutton = cutton;
    }

    public void setBestScore(int bestScore) {
        this.bestScore = bestScore;
    }

    public void setEquip(equip equip) {
        Equip = equip;
    }

    public void setLevel(int level) {
        Level = level;
    }

    public void setEventCoins(int eventCoins) {
        EventCoins = eventCoins;
    }

    public void setSkins(List<Integer> skins) {
        this.skins = skins;
    }

}
