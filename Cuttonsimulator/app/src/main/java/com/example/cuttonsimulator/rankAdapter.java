package com.example.cuttonsimulator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;


public class rankAdapter extends ArrayAdapter<rankItems>
{
    private  Context context;
    private ArrayList<rankItems> list;
    public rankAdapter(@NonNull Context context, ArrayList<rankItems> list) {
        super(context, R.layout.rank,list);
        this.context = context;
        this.list=list;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = ((AppCompatActivity) context).getLayoutInflater();
        // create view object from custom layout xml
        View view = layoutInflater.inflate(R.layout.rank, parent, false);
        // get a reference to a sport object
        rankItems f = this.list.get(position);
        // references to inflated view elements
        // custom layout views
        TextView rank = view.findViewById(R.id.rank);
        TextView name = view.findViewById(R.id.name);
        TextView amount = view.findViewById(R.id.amount);
        // get the element name and image resource id
        rank.setText(String.valueOf(f.getRank()));
        name.setText(String.valueOf(f.getName()));
        amount.setText(String.valueOf(f.getAmount()));
        return view;
    }
}
