package com.example.cuttonsimulator;

public class equip
{
    private int lastEquipped;
    private int id;

    public equip(){}

    public equip(int lastEquipped, int id)
    {
        this.lastEquipped = lastEquipped;
        this.id = id;
    }

    public void setLastEquipped(int lastEquipped) {
        this.lastEquipped = lastEquipped;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLastEquipped() {
        return lastEquipped;
    }

    public int getId() {
        return id;
    }
}
