package com.example.cuttonsimulator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Movie;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.os.Handler;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class GameCutton extends AppCompatActivity  implements View.OnClickListener {
    //Strings.
    private String name;

    //Database.
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    //TextViews.
    private TextView uName;
    private TextView uCutton;
    private TextView uLevel;
    private TextView uEventCoins;
    private TextView Score;
    private TextView bestScore;
    private TextView shopET;
    private TextView returnET;
    private TextView cscoreET;
    private TextView bscoreET;
    private TextView gameoverET;

    //backgrounds.

    //sounds.
    MediaPlayer music1;
    MediaPlayer boss1;
    MediaPlayer boss2;
    MediaPlayer boss3;
    MediaPlayer collect;
    MediaPlayer gameover;


    //buttons.
    Button Shop;
    Button Inventory;
    Button Game;
    Button ranks;
    Button quests;
    Button shopE;
    Button returnE;

    //images.
    ImageView uCart;
    ImageView uCharacter;
    ImageView uBody;
    ImageView cotton1;
    ImageView cotton2;
    ImageView cotton3;
    ImageView cotton4;
    ImageView cotton5;
    ImageView heart1;
    ImageView heart2;
    ImageView heart3;

    //layouts.
    RelativeLayout llGame;
    LinearLayout llData;
    LinearLayout llBtns;

    //classes & arrays.
    CottonImg cottons[] = new CottonImg[5];
    ImageView images[] = new ImageView[5];
    String[] splited;
    String[] split;
    upgradesManager UM;

    //threads:
    private Thread spawner;
    private Thread FallCotton;
    private boolean threadsFlag = true;
    public int bossFlag = 0;
    public boolean musicActive = false;
    public boolean IsBoss = false;

    //etc
    Random random;
    ArrayList<upgrades> lstU;
    int toSpawn = 0, spawnerY = 0, currSpawned = 0, ground = 0, value = 0, hp = 3, score = 0, lastBest = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_cutton);
        //removed the video for now, thats for playing the video background
        /*
        VideoView videoview = (VideoView) findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.background1);
        videoview.setVideoURI(uri);
        videoview.start();
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });*/
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        //Layouts
        //data layout.
        llData = (LinearLayout)findViewById(R.id.dataLL);
        ViewGroup.LayoutParams lp1 = llData.getLayoutParams();
        lp1.height = 50;
        llData.setLayoutParams(lp1);
        //game layout.
        llGame = (RelativeLayout)findViewById(R.id.sizeL);
        ViewGroup.LayoutParams lp = llGame.getLayoutParams();
        lp.height = height - 230;
        ground = height - 180; //end of game layout (start of btns layout).
        spawnerY = -220; //start of game layout (end of data layout).
        llGame.setLayoutParams(lp);
        //btns layout
        llBtns = (LinearLayout)findViewById(R.id.llButtons);
        ViewGroup.LayoutParams lp2 = llBtns.getLayoutParams();
        lp2.height = 180;
        llBtns.setLayoutParams(lp2);

        //set the hearts
        heart1 = findViewById(R.id.heart1);
        heart2 = findViewById(R.id.heart2);
        heart3 = findViewById(R.id.heart3);

        //endGame stuff:
        shopE = findViewById(R.id.shopHiddenB);
        shopE.setOnClickListener(this);
        shopET = findViewById(R.id.shopHiddenT);
        returnE = findViewById(R.id.rtrnB);
        returnE.setOnClickListener(this);
        returnET = findViewById(R.id.rtrnT);
        cscoreET = findViewById(R.id.cscorehidden);
        bscoreET = findViewById(R.id.bscorehidden);
        gameoverET = findViewById(R.id.gameoverT);

        //set the buttons.
        Shop = findViewById(R.id.shopBtn);
        Shop.setWidth(displayMetrics.widthPixels / 6);
        Shop.setOnClickListener(this);
        Shop.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                threadsFlag = false;
                if(music1.isPlaying())
                {
                    music1.stop();
                }
                if(boss1.isPlaying())
                {
                    boss1.stop();
                }
                if(boss2.isPlaying())
                {
                    boss2.stop();
                }
                if(boss3.isPlaying())
                {
                    boss3.stop();
                }
                finish();
                Intent intent=new Intent(com.example.cuttonsimulator.GameCutton.this, Shop.class);
                startActivity(intent);
                return false;
            }
        });
        Inventory = findViewById(R.id.inventoryBtn);
        Inventory.setWidth(displayMetrics.widthPixels / 6);
        Inventory.setOnClickListener(this);
        Inventory.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                //not ready yet.
                threadsFlag = false;
                if(music1.isPlaying())
                {
                    music1.stop();
                }
                if(boss1.isPlaying())
                {
                    boss1.stop();
                }
                if(boss2.isPlaying())
                {
                    boss2.stop();
                }
                if(boss3.isPlaying())
                {
                    boss3.stop();
                }
                finish();
                Intent intent=new Intent(com.example.cuttonsimulator.GameCutton.this, Inventory.class);
                startActivity(intent);
                return false;
            }
        });
        Game = findViewById(R.id.gameBtn);
        Game.setWidth((displayMetrics.widthPixels / 6) * 2);
        Game.setOnClickListener(this);
        ranks = findViewById(R.id.ranksBtn);
        ranks.setWidth(displayMetrics.widthPixels / 6);
        ranks.setOnClickListener(this);
        ranks.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                threadsFlag = false;
                if(music1.isPlaying())
                {
                    music1.stop();
                }
                if(boss1.isPlaying())
                {
                    boss1.stop();
                }
                if(boss2.isPlaying())
                {
                    boss2.stop();
                }
                if(boss3.isPlaying())
                {
                    boss3.stop();
                }
                finish();
                Intent intent=new Intent(com.example.cuttonsimulator.GameCutton.this, ranking.class);
                startActivity(intent);
                return false;
            }
        });
        quests = findViewById(R.id.questBtn);
        quests.setWidth(displayMetrics.widthPixels / 6);
        quests.setOnClickListener(this);
        quests.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                //not ready yet.
                threadsFlag = false;
                if(music1.isPlaying())
                {
                    music1.stop();
                }
                if(boss1.isPlaying())
                {
                    boss1.stop();
                }
                if(boss2.isPlaying())
                {
                    boss2.stop();
                }
                if(boss3.isPlaying())
                {
                    boss3.stop();
                }
                finish();
                //move to the quests window
                Intent intent=new Intent(com.example.cuttonsimulator.GameCutton.this, quests.class);
                startActivity(intent);
                return false;
            }
        });
        //set all the editTexts.
        uName = findViewById(R.id.Name);
        uName.setWidth(displayMetrics.widthPixels / 4);
        uCutton = findViewById(R.id.cutton);
        uCutton.setWidth(displayMetrics.widthPixels / 4);
        uLevel = findViewById(R.id.Level);
        uLevel.setWidth(displayMetrics.widthPixels / 4);
        uEventCoins = findViewById(R.id.eventcoins);
        uEventCoins.setWidth(displayMetrics.widthPixels / 4);

        uCart = findViewById(R.id.cart);
        uCharacter = findViewById(R.id.character);
        uBody = findViewById(R.id.body);
        //set the music:
        music1 = MediaPlayer.create(getApplicationContext(), R.raw.music1);
        boss1 = MediaPlayer.create(getApplicationContext(), R.raw.boss1s);
        boss2 = MediaPlayer.create(getApplicationContext(), R.raw.boss2);
        boss3 = MediaPlayer.create(getApplicationContext(), R.raw.boss3);
        gameover = MediaPlayer.create(getApplicationContext(), R.raw.gameover);
        collect = MediaPlayer.create(getApplicationContext(), R.raw.collectsound);
        lstU = new ArrayList<>();

        bestScore = findViewById(R.id.bestScore);
        Score = findViewById(R.id.currScore);
        //according to the username set all the needed data for the user (like money, cutton, items and stuff).
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        Query q=myRef.child("Users");
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                SharedPreferences sp1=getSharedPreferences("Login",0 );

                if(sp1.getString("Unm", null) != null)
                    name = sp1.getString("Unm", null);
                for(DataSnapshot snp : snapshot.getChildren())
                {
                    Users user = snp.getValue(Users.class);
                    if(user.getName().equals(name))
                    {
                        uName.setText("Name: " + user.getName());
                        bestScore.setText("Best Score: " + String.valueOf(user.getData().getBestScore()));
                        uLevel.setText("Level: " + String.valueOf(user.getData().getLevel()));
                        uCutton.setText("Cutton: " + String.valueOf(user.getData().getCutton()));
                        uEventCoins.setText("EventCoins: " + String.valueOf(user.getData().getEventCoins()));
                        uCart.setImageResource(user.getData().getSkins().get(user.getData().getEquip().getLastEquipped()));
                        uCharacter.setImageResource(user.getData().getCharacters().get(user.getData().getEquipC().getLastEquipped()));
                        uBody.setImageResource(user.getData().getBody().get(user.getData().getEquipB().getLastEquipped()));
                        UM = user.getData().getUpgradesManager();
                        for(int i = 0; i < UM.getUp().size(); i++)
                        {
                            lstU.add(UM.getUp().get(i));
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        //set the cottons.
        cotton1 = findViewById(R.id.cotton1);
        cotton1.getLayoutParams().height = width / 5;
        cottons[0] = new CottonImg(false, 1, 2);
        cotton1.setY(-220);
        images[0] = cotton1;
        cotton2 = findViewById(R.id.cotton2);
        cotton2.getLayoutParams().height = width / 5;
        cottons[1] = new CottonImg(false, 1, 2);
        images[1] = cotton2;
        cotton2.setY(-220);
        cotton3 = findViewById(R.id.cotton3);
        cotton3.getLayoutParams().height = width / 5;
        cottons[2] = new CottonImg(false, 1, 2);
        cotton3.setY(-220);
        images[2] = cotton3;
        cotton4 = findViewById(R.id.cotton4);
        cotton4.getLayoutParams().height = width / 5;
        cottons[3] = new CottonImg(false, 1, 2);
        cotton4.setY(-220);
        images[3] = cotton4;
        cotton5 = findViewById(R.id.cotton5);
        cotton5.getLayoutParams().height = width / 5;
        cottons[4] = new CottonImg(false, 1, 2);
        cotton5.setY(-220);
        images[4] = cotton5;


        //start the game.
        music1.setLooping(true);
        music1.start();
        //paintView pv = new paintView(this);
        //llGame.addView(pv);
        spawner = new Thread() {
            //spawn x = height - Data height, groundY = height - Btns height, cartY = height - (Btns height + cart Height).
            @Override
            public void run() {
                try {
                    while(!GameCutton.this.isFinishing() && !GameCutton.this.isDestroyed() && threadsFlag) {
                        sleep(1000);
                        //spawn random cotton(make it visible) and make it fall, after 1-3 seconds spawn next one.
                        if(currSpawned < 5) //check if one of them isn't spawned yet.
                        {
                            random = new Random();
                            toSpawn = random.nextInt(5);
                            if(!cottons[toSpawn].isSpawned())
                            {
                                //spawn
                                currSpawned = currSpawned + 1;
                                cottons[toSpawn].setSpawned(true);
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        FallCotton = new Thread() {
            //spawn x = height - Data height, groundY = height - Btns height, cartY = height - (Btns height + cart Height).
            @Override
            public void run() {
                try {
                    while(!GameCutton.this.isFinishing() && !GameCutton.this.isDestroyed() && threadsFlag) {
                        sleep(10);
                        for(int i = 0; i < 5; i++)
                        {
                            if(cottons[i].isSpawned())
                            {
                                if(score >= 10 && score < 20)
                                {
                                    images[i].setY(images[i].getY() + cottons[i].getSpeed() + 6);
                                    if(!musicActive)
                                    {
                                        music1.pause();
                                        boss1.setLooping(true);
                                        boss1.start();
                                        musicActive = true;
                                        //change cutton to cop.
                                        runOnUiThread(() -> {
                                            //respawn all cotton before boss.
                                            currSpawned = 0;
                                            for(int j = 0; j < cottons.length; j++)
                                            {
                                                cottons[j].setSpawned(false);
                                                images[j].setY(spawnerY);
                                            }
                                            //do message that says BOSS COMING.
                                            cotton1.setImageResource(R.drawable.boss1);
                                            cottons[0].setAmount(cottons[0].getAmount() * 2);
                                            cotton2.setImageResource(R.drawable.boss1);
                                            cottons[1].setAmount(cottons[1].getAmount() * 2);
                                            cotton3.setImageResource(R.drawable.boss1);
                                            cottons[2].setAmount(cottons[2].getAmount() * 2);
                                            cotton4.setImageResource(R.drawable.boss1);
                                            cottons[3].setAmount(cottons[3].getAmount() * 2);
                                            cotton5.setImageResource(R.drawable.boss1);
                                            cottons[4].setAmount(cottons[4].getAmount() * 2);
                                        });
                                        IsBoss = true;
                                    }
                                }
                                else if(score >= 30 && score < 50)
                                {
                                    images[i].setY(images[i].getY() + cottons[i].getSpeed() + 8);
                                    if(!musicActive)
                                    {
                                        music1.pause();
                                        boss2.setLooping(true);
                                        boss2.start();
                                        musicActive = true;
                                        runOnUiThread(() -> {
                                            //respawn all cotton before boss.
                                            currSpawned = 0;
                                            for(int j = 0; j < cottons.length; j++)
                                            {
                                                cottons[j].setSpawned(false);
                                                images[j].setY(spawnerY);
                                            }
                                            //do message that says BOSS COMING.
                                            cotton1.setImageResource(R.drawable.boss1);
                                            cottons[0].setAmount(cottons[0].getAmount() * 3);
                                            cotton2.setImageResource(R.drawable.boss1);
                                            cottons[1].setAmount(cottons[1].getAmount() * 3);
                                            cotton3.setImageResource(R.drawable.boss1);
                                            cottons[2].setAmount(cottons[2].getAmount() * 3);
                                            cotton4.setImageResource(R.drawable.boss1);
                                            cottons[3].setAmount(cottons[3].getAmount() * 3);
                                            cotton5.setImageResource(R.drawable.boss1);
                                            cottons[4].setAmount(cottons[4].getAmount() * 3);
                                        });
                                        IsBoss = true;
                                    }
                                }
                                else if(score >= 65)
                                {
                                    images[i].setY(images[i].getY() + cottons[i].getSpeed() + 11);
                                    if(!musicActive)
                                    {
                                        music1.pause();
                                        boss3.setLooping(true);
                                        boss3.start();
                                        IsBoss = true;
                                        musicActive = true;
                                        runOnUiThread(new Runnable()
                                        {
                                            @Override
                                            public void run() {
                                                //respawn all cotton before boss.
                                                currSpawned = 0;
                                                for(int j = 0; j < cottons.length; j++)
                                                {
                                                    cottons[j].setSpawned(false);
                                                    images[j].setY(spawnerY);
                                                }
                                                //do message that says BOSS COMING.
                                                cotton1.setImageResource(R.drawable.boss1);
                                                cottons[0].setAmount(cottons[0].getAmount() * 4);
                                                cotton2.setImageResource(R.drawable.boss1);
                                                cottons[1].setAmount(cottons[1].getAmount() * 4);
                                                cotton3.setImageResource(R.drawable.boss1);
                                                cottons[2].setAmount(cottons[2].getAmount() * 4);
                                                cotton4.setImageResource(R.drawable.boss1);
                                                cottons[3].setAmount(cottons[3].getAmount() * 4);
                                                cotton5.setImageResource(R.drawable.boss1);
                                                cottons[4].setAmount(cottons[4].getAmount() * 4);
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    images[i].setY(images[i].getY() + cottons[i].getSpeed());
                                    if(!music1.isPlaying())
                                    {
                                        //means the song is over play another one or replay the current.
                                        music1.setLooping(true);
                                        music1.start();
                                    }
                                    if(score >= 20 && bossFlag == 0)
                                    {
                                        boss1.pause();
                                        IsBoss = false;
                                        bossFlag++;
                                        musicActive = false;
                                        //can change music 1 here to another one.
                                        music1.start();
                                        runOnUiThread(() -> {
                                            //respawn all cotton before boss.
                                            currSpawned = 0;
                                            for(int j = 0; j < cottons.length; j++)
                                            {
                                                cottons[j].setSpawned(false);
                                                images[j].setY(spawnerY);
                                            }
                                            //do message that says BOSS COMING.
                                            cotton1.setImageResource(R.drawable.cotton);
                                            cotton2.setImageResource(R.drawable.cotton);
                                            cotton3.setImageResource(R.drawable.cotton);
                                            cotton4.setImageResource(R.drawable.cotton);
                                            cotton5.setImageResource(R.drawable.cotton);
                                            cottons[0].setAmount(cottons[0].getAmount() / 2);
                                            cottons[1].setAmount(cottons[1].getAmount() / 2);
                                            cottons[2].setAmount(cottons[2].getAmount() / 2);
                                            cottons[3].setAmount(cottons[3].getAmount() / 2);
                                            cottons[4].setAmount(cottons[4].getAmount() / 2);
                                        });
                                    }
                                    else if(score >= 50 && bossFlag == 1)
                                    {
                                        boss2.pause();
                                        IsBoss = false;
                                        bossFlag++;
                                        musicActive = false;
                                        music1.start();
                                        runOnUiThread(new Runnable()
                                        {
                                            @Override
                                            public void run() {
                                                //respawn all cotton before boss.
                                                currSpawned = 0;
                                                for(int j = 0; j < cottons.length; j++)
                                                {
                                                    cottons[j].setSpawned(false);
                                                    images[j].setY(spawnerY);
                                                }
                                                //do message that says BOSS COMING.
                                                cotton1.setImageResource(R.drawable.cotton);
                                                cotton2.setImageResource(R.drawable.cotton);
                                                cotton3.setImageResource(R.drawable.cotton);
                                                cotton4.setImageResource(R.drawable.cotton);
                                                cotton5.setImageResource(R.drawable.cotton);
                                                cottons[0].setAmount(cottons[0].getAmount() / 3);
                                                cottons[1].setAmount(cottons[1].getAmount() / 3);
                                                cottons[2].setAmount(cottons[2].getAmount() / 3);
                                                cottons[3].setAmount(cottons[3].getAmount() / 3);
                                                cottons[4].setAmount(cottons[4].getAmount() / 3);
                                            }
                                        });
                                    }
                                }
                                //check if it hit the cart (x in range of cart range, for example if cart x = 20 and its width is 10 so 15-25 = cart)
                                if((images[i].getY() + uCart.getHeight() - uCart.getY() >= 0 && images[i].getY() + uCart.getHeight() - uCart.getY() - uCart.getHeight() / 2 < 0) && (Math.abs(Math.abs(uCart.getX()) - Math.abs(images[i].getX())) <= uCart.getWidth() / 2))
                                {
                                    if(IsBoss)
                                    {
                                        currSpawned--;
                                        cottons[i].setSpawned(false);
                                        images[i].setY(spawnerY);
                                        //remove 1 hp, if the user reach 0 hp so its game over.
                                        hp = hp - 1;
                                        if(hp == 2)
                                        {
                                            //replace heart1 src to be empty.
                                            runOnUiThread(new Runnable()
                                            {
                                                @Override
                                                public void run() {
                                                    heart1.setImageResource(R.drawable.empty);
                                                }
                                            });
                                        }
                                        else if(hp == 1)
                                        {
                                            //replace heart2 src to be empty.
                                            runOnUiThread(new Runnable() {

                                                @Override
                                                public void run() {
                                                    heart2.setImageResource(R.drawable.empty);
                                                }
                                            });
                                        }
                                        if(hp == 0)
                                        {
                                            //replace heart3 src to be empty.
                                            runOnUiThread(() -> {
                                                heart3.setImageResource(R.drawable.empty);
                                                threadsFlag = false;
                                                llGame.removeAllViews();
                                                music1.pause();
                                                music1.reset();
                                                boss1.pause();
                                                boss1.reset();
                                                boss2.pause();
                                                boss2.reset();
                                                boss3.pause();
                                                boss3.reset();
                                                IsBoss = false;
                                                gameover = MediaPlayer.create(getApplicationContext(), R.raw.gameover);
                                                gameover.start();
                                                llGame.addView(returnE);
                                                llGame.addView(returnET);
                                                llGame.addView(shopE);
                                                llGame.addView(shopET);
                                                llGame.addView(cscoreET);
                                                llGame.addView(bscoreET);
                                                llGame.addView(gameoverET);
                                                cscoreET.setText("Current Score: " + String.valueOf(score));
                                                score = 0;
                                                bscoreET.setText(bestScore.getText());
                                                gameoverET.setVisibility(View.VISIBLE);
                                                cscoreET.setVisibility(View.VISIBLE);
                                                bscoreET.setVisibility(View.VISIBLE);
                                                returnE.setVisibility(View.VISIBLE);
                                                returnET.setVisibility(View.VISIBLE);
                                                shopE.setVisibility(View.VISIBLE);
                                                shopET.setVisibility(View.VISIBLE);
                                            });
                                            //change the layout to game over layout with reset&shop button & show score and best score.
                                        }
                                    }
                                    else
                                    {
                                        //in the cart so give points and set to spawn.
                                        collect.start();
                                        currSpawned--;
                                        cottons[i].setSpawned(false);
                                        images[i].setY(spawnerY);
                                        //add the points:
                                        splited = uCutton.getText().toString().split("\\s+");
                                        value =  Integer.valueOf(splited[1]) + cottons[0].getAmount();
                                        score++;
                                        Score.setText("Score: " + String.valueOf(score));
                                        uCutton.setText("Cottons: " + String.valueOf(value));
                                        //set score to be best if it is higher.
                                        split = bestScore.getText().toString().split("\\s+");
                                        lastBest =  Integer.valueOf(split[2]);
                                        HashMap Usr = new HashMap();
                                        myRef.child("Users").updateChildren(Usr).addOnCompleteListener(new OnCompleteListener() {
                                            @Override
                                            public void onComplete(@NonNull Task task)
                                            {
                                                splited = uCutton.getText().toString().split("\\s+");
                                                value =  Integer.valueOf(splited[1]) + cottons[0].getAmount();
                                                myRef.child("Users").child(name).child("data").child("cutton").setValue(value);
                                                if(score > lastBest)
                                                {
                                                    bestScore.setText("Best Score: " + score);
                                                    myRef.child("Users").child(name).child("data").child("bestScore").setValue(score);
                                                }
                                            }
                                        });
                                        //myRef.child("Users").child(name).child("data").child("cutton").setValue(value);
                                    }
                                }
                                if(images[i].getY() - ground >= 0)
                                {
                                    if(IsBoss)
                                    {
                                        //in the cart so give points and set to spawn.
                                        collect = MediaPlayer.create(getApplicationContext(), R.raw.collectsound);
                                        collect.start();
                                        currSpawned--;
                                        cottons[i].setSpawned(false);
                                        images[i].setY(spawnerY);
                                        //add the points:
                                        splited = uCutton.getText().toString().split("\\s+");
                                        value =  Integer.valueOf(splited[1]) + cottons[0].getAmount();
                                        score++;
                                        Score.setText("Score: " + String.valueOf(score));
                                        uCutton.setText("Cottons: " + String.valueOf(value));
                                        //set score to be best if it is higher.
                                        split = bestScore.getText().toString().split("\\s+");
                                        lastBest =  Integer.valueOf(split[2]);
                                        HashMap Usr = new HashMap();
                                        myRef.child("Users").updateChildren(Usr).addOnCompleteListener(new OnCompleteListener() {
                                            @Override
                                            public void onComplete(@NonNull Task task)
                                            {
                                                splited = uCutton.getText().toString().split("\\s+");
                                                value =  Integer.valueOf(splited[1]) + cottons[0].getAmount();
                                                myRef.child("Users").child(name).child("data").child("cutton").setValue(value);
                                                if(score > lastBest)
                                                {
                                                    bestScore.setText("Best Score: " + score);
                                                    myRef.child("Users").child(name).child("data").child("bestScore").setValue(score);
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        //hit the ground so set to spawn.
                                        currSpawned--;
                                        cottons[i].setSpawned(false);
                                        images[i].setY(spawnerY);
                                        //remove 1 hp, if the user reach 0 hp so its game over.
                                        hp = hp - 1;
                                        if(hp == 2)
                                        {
                                            //replace heart1 src to be empty.
                                            runOnUiThread(new Runnable()
                                            {
                                                @Override
                                                public void run() {
                                                    GameCutton.this.heart1.setImageResource(R.drawable.empty);
                                                }
                                            });
                                        }
                                        else if(hp == 1)
                                        {
                                            //replace heart2 src to be empty.
                                            runOnUiThread(new Runnable() {

                                                @Override
                                                public void run() {
                                                    GameCutton.this.heart2.setImageResource(R.drawable.empty);
                                                }
                                            });
                                        }
                                        if(hp == 0)
                                        {
                                            //replace heart3 src to be empty.
                                            runOnUiThread(() -> {
                                                GameCutton.this.heart3.setImageResource(R.drawable.empty);
                                                threadsFlag = false;
                                                llGame.removeAllViews();
                                                music1.pause();
                                                music1.reset();
                                                boss1.pause();
                                                boss1.reset();
                                                boss2.pause();
                                                boss2.reset();
                                                boss3.pause();
                                                boss3.reset();
                                                IsBoss = false;
                                                gameover = MediaPlayer.create(getApplicationContext(), R.raw.gameover);
                                                gameover.start();
                                                llGame.addView(returnE);
                                                llGame.addView(returnET);
                                                llGame.addView(shopE);
                                                llGame.addView(shopET);
                                                llGame.addView(cscoreET);
                                                llGame.addView(bscoreET);
                                                llGame.addView(gameoverET);
                                                cscoreET.setText("Current Score: " + String.valueOf(score));
                                                score = 0;
                                                bscoreET.setText(bestScore.getText());
                                                gameoverET.setVisibility(View.VISIBLE);
                                                cscoreET.setVisibility(View.VISIBLE);
                                                bscoreET.setVisibility(View.VISIBLE);
                                                returnE.setVisibility(View.VISIBLE);
                                                returnET.setVisibility(View.VISIBLE);
                                                shopE.setVisibility(View.VISIBLE);
                                                shopET.setVisibility(View.VISIBLE);
                                            });
                                            //change the layout to game over layout with reset&shop button & show score and best score.
                                        }
                                    }
                                }

                            }
                        }
                        //check if it hit ground then make it invisible again.
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Handler handler = new Handler();
        int delay = 1000; //milliseconds

        handler.postDelayed(new Runnable(){
            public void run(){
                if(!lstU.isEmpty())//checking if the data is loaded or not
                {
                    runOnUiThread(() -> {
                        //set the cottons.
                        cotton1 = findViewById(R.id.cotton1);
                        cotton1.getLayoutParams().height = width / 5;
                        cottons[0] = new CottonImg(false, lstU.get(1).getAmount(), lstU.get(0).getAmount());
                        cotton1.setY(-220);
                        images[0] = cotton1;
                        cotton2 = findViewById(R.id.cotton2);
                        cotton2.getLayoutParams().height = width / 5;
                        cottons[1] = new CottonImg(false, lstU.get(1).getAmount(), lstU.get(0).getAmount());
                        images[1] = cotton2;
                        cotton2.setY(-220);
                        cotton3 = findViewById(R.id.cotton3);
                        cotton3.getLayoutParams().height = width / 5;
                        cottons[2] = new CottonImg(false, lstU.get(1).getAmount(), lstU.get(0).getAmount());
                        cotton3.setY(-220);
                        images[2] = cotton3;
                        cotton4 = findViewById(R.id.cotton4);
                        cotton4.getLayoutParams().height = width / 5;
                        cottons[3] = new CottonImg(false, lstU.get(1).getAmount(), lstU.get(0).getAmount());
                        cotton4.setY(-220);
                        images[3] = cotton4;
                        cotton5 = findViewById(R.id.cotton5);
                        cotton5.getLayoutParams().height = width / 5;
                        cottons[4] = new CottonImg(false, lstU.get(1).getAmount(), lstU.get(0).getAmount());
                        cotton5.setY(-220);
                        images[4] = cotton5;
                        spawner.start();
                        FallCotton.start();
                    });
                }
                else
                    handler.postDelayed(this, delay);
                }
            }, delay);
    }

    @Override
    public void onClick(View view)
    {
        if(view == Shop) //completed skins shop, still possible to add more skins.
        {
            threadsFlag = false;
            if(music1.isPlaying())
            {
                music1.stop();
            }
            if(boss1.isPlaying())
            {
                boss1.stop();
            }
            if(boss2.isPlaying())
            {
                boss2.stop();
            }
            if(boss3.isPlaying())
            {
                boss3.stop();
            }
            finish();
            Intent intent=new Intent(com.example.cuttonsimulator.GameCutton.this, Shop.class);
            startActivity(intent);
        }
        else if(view == Inventory)
        {
            //not ready yet.

            threadsFlag = false;
            if(music1.isPlaying())
            {
                music1.stop();
            }
            if(boss1.isPlaying())
            {
                boss1.stop();
            }
            if(boss2.isPlaying())
            {
                boss2.stop();
            }
            if(boss3.isPlaying())
            {
                boss3.stop();
            }
            finish();
            Intent intent=new Intent(com.example.cuttonsimulator.GameCutton.this, Inventory.class);
            startActivity(intent);
        }
        else if(view == ranks)
        {
            threadsFlag = false;
            if(music1.isPlaying())
            {
                music1.stop();
            }
            if(boss1.isPlaying())
            {
                boss1.stop();
            }
            if(boss2.isPlaying())
            {
                boss2.stop();
            }
            if(boss3.isPlaying())
            {
                boss3.stop();
            }
            finish();
            Intent intent=new Intent(com.example.cuttonsimulator.GameCutton.this, ranking.class);
            startActivity(intent);
        }
        else if(view == quests)
        {
            //not ready yet.
            threadsFlag = false;
            if(music1.isPlaying())
            {
                music1.stop();
            }
            if(boss1.isPlaying())
            {
                boss1.stop();
            }
            if(boss2.isPlaying())
            {
                boss2.stop();
            }
            if(boss3.isPlaying())
            {
                boss3.stop();
            }
            finish();
            Intent intent=new Intent(com.example.cuttonsimulator.GameCutton.this, quests.class);
            startActivity(intent);
        }
        else if(view == shopE)
        {
            threadsFlag = false;
            if(music1.isPlaying())
            {
                music1.stop();
            }
            if(boss1.isPlaying())
            {
                boss1.stop();
            }
            if(boss2.isPlaying())
            {
                boss2.stop();
            }
            if(boss3.isPlaying())
            {
                boss3.stop();
            }
            finish();
            Intent intent=new Intent(com.example.cuttonsimulator.GameCutton.this, Shop.class);
            startActivity(intent);
        }
        else if(view == returnE)
        {
            finish();
            startActivity(getIntent());
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        uCart.setX(event.getX());
        uCharacter.setX(event.getX() + 140);
        uBody.setX(uCharacter.getX());
        return false;
    }
    /*
    String[] splited = uCutton.getText().toString().split("\\s+");
    int value =  Integer.valueOf(splited[1]);
    myRef.child("Users").child(name).child("data").child("cutton").setValue(value);
    splited = uEventCoins.getText().toString().split("\\s+");
    value =  Integer.valueOf(splited[1]);
    myRef.child("Users").child(name).child("data").child("eventCoins").setValue(value);
    splited = uLevel.getText().toString().split("\\s+");
    value =  Integer.valueOf(splited[1]);
    myRef.child("Users").child(name).child("data").child("level").setValue(value);
    */
}