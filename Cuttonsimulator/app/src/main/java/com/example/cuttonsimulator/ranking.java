package com.example.cuttonsimulator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ranking extends AppCompatActivity implements View.OnClickListener {

    //layouts.
    LinearLayout llRanking;
    LinearLayout llData;
    LinearLayout llBtns;

    //layouts btns.
    Button Shop;
    Button Inventory;
    Button Game;
    Button ranks;
    Button quests;

    //layout textView.
    private TextView uName;
    private TextView uCutton;
    private TextView uLevel;
    private TextView uEventCoins;

    //Strings.
    private String nameU;

    //list vars.
    ListView ls;
    ArrayList<rankItems> lst;
    rankAdapter adapter;

    //buttons.
    Button cotton;
    Button level;
    Button score;
    Button EventCoins;

    //text.
    TextView TopText;

    //Database.
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        //Layouts
        //data layout.
        llData = (LinearLayout)findViewById(R.id.dataLLRanking);
        ViewGroup.LayoutParams lp1 = llData.getLayoutParams();
        lp1.height = 50;
        llData.setLayoutParams(lp1);
        //game layout.
        llRanking = (LinearLayout)findViewById(R.id.sizeLRanking);
        ViewGroup.LayoutParams lp = llRanking.getLayoutParams();
        lp.height = height - 230;
        llRanking.setLayoutParams(lp);
        //btns layout
        llBtns = (LinearLayout)findViewById(R.id.llbtnsRanking);
        ViewGroup.LayoutParams lp2 = llBtns.getLayoutParams();
        lp2.height = 180;
        llBtns.setLayoutParams(lp2);

        Shop = findViewById(R.id.shopBtnRanking);
        Shop.setWidth(displayMetrics.widthPixels / 6);
        Shop.setOnClickListener(this);
        Inventory = findViewById(R.id.inventoryBtnRanking);
        Inventory.setWidth(displayMetrics.widthPixels / 6);
        Inventory.setOnClickListener(this);
        Game = findViewById(R.id.gameBtnRanking);
        Game.setWidth((displayMetrics.widthPixels / 6) * 2);
        Game.setOnClickListener(this);
        ranks = findViewById(R.id.ranksBtnRanking);
        ranks.setWidth(displayMetrics.widthPixels / 6);
        ranks.setOnClickListener(this);
        quests = findViewById(R.id.questsBtnRanking);
        quests.setWidth(displayMetrics.widthPixels / 6);
        quests.setOnClickListener(this);
        //set all the editTexts.
        uName = findViewById(R.id.NameRanking);
        uName.setWidth(displayMetrics.widthPixels / 4);
        uCutton = findViewById(R.id.cuttonRanking);
        uCutton.setWidth(displayMetrics.widthPixels / 4);
        uLevel = findViewById(R.id.LevelRanking);
        uLevel.setWidth(displayMetrics.widthPixels / 4);
        uEventCoins = findViewById(R.id.eventcoinsRanking);
        uEventCoins.setWidth(displayMetrics.widthPixels / 4);

        //btns.
        cotton = findViewById(R.id.cottonBtn);
        cotton.setOnClickListener(this);
        level = findViewById(R.id.levelBtn);
        level.setOnClickListener(this);
        score = findViewById(R.id.scoreBtn);
        score.setOnClickListener(this);
        EventCoins = findViewById(R.id.eventBtn);
        EventCoins.setOnClickListener(this);
        //txt.
        TopText = findViewById(R.id.topTxt);

        //firebase.
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        //listView.
        ls = findViewById(R.id.rankList);

        //get default ranking: score.
        Query q=myRef.child("Users");
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                SharedPreferences sp1=getSharedPreferences("Login",0 );
                if(sp1.getString("Unm", null) != null)
                    nameU = sp1.getString("Unm", null);
                int rank = 0, amount = 0;
                String name;
                rankItems r;
                lst = new ArrayList<rankItems>();
                for(DataSnapshot snp : snapshot.getChildren())
                {
                    Users user = snp.getValue(Users.class);
                    if(user.getName().equals(nameU))
                    {
                        uName.setText("Name: " + user.getName());
                        uLevel.setText("Level: " + String.valueOf(user.getData().getLevel()));
                        uCutton.setText("Cutton: " + String.valueOf(user.getData().getCutton()));
                        uEventCoins.setText("EventCoins: " + String.valueOf(user.getData().getEventCoins()));
                    }
                    rank++;
                    name = user.getName();
                    amount = user.getData().getBestScore();
                    r = new rankItems(rank, name, amount);
                    lst.add(r);
                }
                Collections.sort(lst, new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        rankItems r1 = (rankItems) o1;
                        rankItems r2 = (rankItems) o2;
                        return Integer.valueOf(r2.getAmount()).compareTo(r1.getAmount());
                    }
                });
                //set the rank after the sort.
                for(int i = 0; i < lst.size(); i++)
                {
                    lst.get(i).setRank(i + 1);
                }
                adapter = new rankAdapter(ranking.this,lst);
                ls.setAdapter(adapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    @Override
    public void onClick(View view)
    {
        if(view == Game)
        {
            Intent intent=new Intent(com.example.cuttonsimulator.ranking.this, GameCutton.class);
            startActivity(intent);
        }
        else if(view == Inventory)
        {
            //move to the inventory window
            Intent intent=new Intent(com.example.cuttonsimulator.ranking.this, Inventory.class);
            startActivity(intent);
        }
        else if(view == ranks)
        {
            //move to the another1 window
        }
        else if(view == quests)
        {
            //now move to the another2 window
        }
        else if(view == Shop)
        {
            Intent intent=new Intent(com.example.cuttonsimulator.ranking.this, Shop.class);
            startActivity(intent);
        }
        else if(view == cotton)
        {
            TopText.setText("Top cotton:");
            Query q=myRef.child("Users");
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    int rank = 0, amount = 0;
                    String name;
                    rankItems r;
                    lst = new ArrayList<rankItems>();
                    for(DataSnapshot snp : snapshot.getChildren())
                    {
                        Users user = snp.getValue(Users.class);
                        rank++;
                        name = user.getName();
                        amount = user.getData().getCutton();
                        r = new rankItems(rank, name, amount);
                        lst.add(r);
                    }
                    Collections.sort(lst, new Comparator() {
                        @Override
                        public int compare(Object o1, Object o2) {
                            rankItems r1 = (rankItems) o1;
                            rankItems r2 = (rankItems) o2;
                            return Integer.valueOf(r2.getAmount()).compareTo(r1.getAmount());
                        }
                    });
                    //set the rank after the sort.
                    for(int i = 0; i < lst.size(); i++)
                    {
                        lst.get(i).setRank(i + 1);
                    }
                    adapter = new rankAdapter(ranking.this,lst);
                    ls.setAdapter(adapter);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });
        }
        else if(view == level)
        {
            TopText.setText("Top levels:");
            Query q=myRef.child("Users");
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    int rank = 0, amount = 0;
                    String name;
                    rankItems r;
                    lst = new ArrayList<rankItems>();
                    for(DataSnapshot snp : snapshot.getChildren())
                    {
                        Users user = snp.getValue(Users.class);
                        rank++;
                        name = user.getName();
                        amount = user.getData().getLevel();
                        r = new rankItems(rank, name, amount);
                        lst.add(r);
                    }
                    Collections.sort(lst, new Comparator() {
                        @Override
                        public int compare(Object o1, Object o2) {
                            rankItems r1 = (rankItems) o1;
                            rankItems r2 = (rankItems) o2;
                            return Integer.valueOf(r2.getAmount()).compareTo(r1.getAmount());
                        }
                    });
                    //set the rank after the sort.
                    for(int i = 0; i < lst.size(); i++)
                    {
                        lst.get(i).setRank(i + 1);
                    }
                    adapter = new rankAdapter(ranking.this,lst);
                    ls.setAdapter(adapter);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });
        }
        else if(view == score)
        {
            TopText.setText("Top score:");
            Query q=myRef.child("Users");
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    int rank = 0, amount = 0;
                    String name;
                    rankItems r;
                    lst = new ArrayList<rankItems>();
                    for(DataSnapshot snp : snapshot.getChildren())
                    {
                        Users user = snp.getValue(Users.class);
                        rank++;
                        name = user.getName();
                        amount = user.getData().getBestScore();
                        r = new rankItems(rank, name, amount);
                        lst.add(r);
                    }
                    Collections.sort(lst, new Comparator() {
                        @Override
                        public int compare(Object o1, Object o2) {
                            rankItems r1 = (rankItems) o1;
                            rankItems r2 = (rankItems) o2;
                            return Integer.valueOf(r2.getAmount()).compareTo(r1.getAmount());
                        }
                    });
                    //set the rank after the sort.
                    for(int i = 0; i < lst.size(); i++)
                    {
                        lst.get(i).setRank(i + 1);
                    }
                    adapter = new rankAdapter(ranking.this,lst);
                    ls.setAdapter(adapter);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });
        }
        else if(view == EventCoins)
        {
            TopText.setText("Top EventCoins:");
            Query q=myRef.child("Users");
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    int rank = 0, amount = 0;
                    String name;
                    rankItems r;
                    lst = new ArrayList<rankItems>();
                    for(DataSnapshot snp : snapshot.getChildren())
                    {
                        Users user = snp.getValue(Users.class);
                        rank++;
                        name = user.getName();
                        amount = user.getData().getEventCoins();
                        r = new rankItems(rank, name, amount);
                        lst.add(r);
                    }
                    Collections.sort(lst, new Comparator() {
                        @Override
                        public int compare(Object o1, Object o2) {
                            rankItems r1 = (rankItems) o1;
                            rankItems r2 = (rankItems) o2;
                            return Integer.valueOf(r2.getAmount()).compareTo(r1.getAmount());
                        }
                    });
                    //set the rank after the sort.
                    for(int i = 0; i < lst.size(); i++)
                    {
                        lst.get(i).setRank(i + 1);
                    }
                    adapter = new rankAdapter(ranking.this,lst);
                    ls.setAdapter(adapter);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });
        }
    }
}