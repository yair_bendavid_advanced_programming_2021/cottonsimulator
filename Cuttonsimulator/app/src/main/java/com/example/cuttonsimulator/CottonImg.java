package com.example.cuttonsimulator;

public class CottonImg {
    private boolean IsSpawned;
    private int speed;
    private int Amount; //amount of cotton it gives the user.

    public CottonImg(){}

    public CottonImg(boolean isSpawned, int speed, int amount)
    {
        this.IsSpawned = isSpawned;
        this.speed = speed;
        this.Amount = amount;
    }

    public boolean isSpawned() {
        return IsSpawned;
    }

    public int getAmount() {
        return Amount;
    }

    public int getSpeed() {
        return speed;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }

    public void setSpawned(boolean spawned) {
        IsSpawned = spawned;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

}
