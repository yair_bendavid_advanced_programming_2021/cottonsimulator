package com.example.cuttonsimulator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Login extends AppCompatActivity implements View.OnClickListener {
    EditText user;
    EditText pass;
    Button Login;
    Button Register;
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);
        Login = findViewById(R.id.login);
        Register = findViewById(R.id.register);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        SharedPreferences sp = getSharedPreferences("Login",0 );
        if(sp.getString("Unm", null) != null)
            user.setText(sp.getString("Unm", null));
        if(sp.getString("Psw", null) != null)
            pass.setText(sp.getString("Psw", null));
        VideoView videoview = (VideoView) findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.intro);
        videoview.setVideoURI(uri);
        videoview.start();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                user.setVisibility(View.VISIBLE);
                pass.setVisibility(View.VISIBLE);
                Register.setVisibility(View.VISIBLE);
                Login.setVisibility(View.VISIBLE);
            }
        }, 4100);
        Login.setOnClickListener(this);
        Register.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        SharedPreferences sp=getSharedPreferences("Login", 0);
        SharedPreferences.Editor Ed=sp.edit();
        if(view == Login)
        {
            //check if already exist.
            Query q=myRef.child("Users");
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    boolean isExist = false;
                    for(DataSnapshot snp : snapshot.getChildren())
                    {
                        Users u1 = snp.getValue(Users.class);
                        if(u1.getName().equals(user.getText().toString()) && u1.getPassword().equals(pass.getText().toString()))
                        {
                            Ed.putString("Unm",user.getText().toString());
                            Ed.putString("Psw",pass.getText().toString());
                            Ed.commit();
                            isExist = true;
                            Intent intent=new Intent(com.example.cuttonsimulator.Login.this, GameCutton.class);
                            startActivity(intent);
                        }
                    }
                    if(!isExist)
                    {
                        //invalid login details (username or password
                        Toast.makeText(Login.this, "invalid username / password!", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        else if(view == Register)
        {
            Intent intent=new Intent(this, Register.class);
            startActivity(intent);
        }
    }
}