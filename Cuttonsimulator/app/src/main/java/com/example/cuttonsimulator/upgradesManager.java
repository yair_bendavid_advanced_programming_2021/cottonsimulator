package com.example.cuttonsimulator;

import java.util.List;

public class upgradesManager
{
    private List<upgrades> up;

    public upgradesManager(){};

    public upgradesManager(List<upgrades> up)
    {
        this.up = up;
    }

    public List<upgrades> getUp() {
        return up;
    }

    public void setUp(List<upgrades> up) {
        this.up = up;
    }
}
