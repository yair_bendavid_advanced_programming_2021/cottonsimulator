package com.example.cuttonsimulator;

import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import androidx.annotation.NonNull;
        import androidx.annotation.Nullable;
        import androidx.appcompat.app.AppCompatActivity;

        import java.util.ArrayList;


public class SkinsAdapter extends ArrayAdapter<Skins>
{
    private  Context context;
    private ArrayList<Skins> list;
    public SkinsAdapter(@NonNull Context context, ArrayList<Skins> list) {
        super(context, R.layout.skins_layout,list);
        this.context = context;
        this.list=list;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = ((AppCompatActivity) context).getLayoutInflater();
        // create view object from custom layout xml
        View view = layoutInflater.inflate(R.layout.skins_layout, parent, false);
        // get a reference to a sport object
        Skins f = this.list.get(position);
        // references to inflated view elements
        // custom layout views
        TextView Name = view.findViewById(R.id.name);
        TextView Price = view.findViewById(R.id.price);
        TextView owned = view.findViewById(R.id.owned);
        ImageView img = view.findViewById(R.id.img);
        // get the element name and image resource id
        Name.setText(f.getName());
        Price.setText(String.valueOf(f.getPrice()));
        img.setImageResource(f.getImg());
        if(f.isOwned())
        {
            if(f.getLastId() == f.getId())
            {
                owned.setText("Equipped");
            }
            else
            {
                owned.setText("Equip");
            }
        }
        else
        {
            owned.setText("Buy");
        }
        return view;
    }
}
