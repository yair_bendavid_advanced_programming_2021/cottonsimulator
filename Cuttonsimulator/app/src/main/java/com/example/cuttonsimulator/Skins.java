package com.example.cuttonsimulator;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class Skins
{
    private int id;
    private String Name;
    private int Price;
    private Integer drawable;
    private boolean isOwned;
    private int lastId;

    public Skins(int id, String Name, int price, Integer img, boolean isOwned, int lastId)
    {
        this.id = id;
        this.Name = Name;
        this.Price = price;
        this.drawable = img;
        this.isOwned = isOwned;
        this.lastId = lastId;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setOwned(boolean owned) {
        isOwned = owned;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setLastId(int lastId) {
        this.lastId = lastId;
    }

    public void setImg(Integer img) {
        this.drawable = img;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public String getName() {
        return Name;
    }

    public Integer getImg() {
        return drawable;
    }

    public int getId() {
        return id;
    }

    public int getPrice() {
        return Price;
    }

    public int getLastId() {
        return lastId;
    }

    public boolean isOwned() {
        return isOwned;
    }
}
