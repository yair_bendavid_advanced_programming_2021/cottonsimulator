package com.example.cuttonsimulator;

public class Users
{
    private String name;
    private String password;
    private UserData Data;

    public Users()
    {

    }

    public Users(String name, String password, UserData data)
    {
        this.name = name;
        this.password = password;
        this.Data = data;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public UserData getData() {
        return Data;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setData(UserData data) {
        Data = data;
    }
}
