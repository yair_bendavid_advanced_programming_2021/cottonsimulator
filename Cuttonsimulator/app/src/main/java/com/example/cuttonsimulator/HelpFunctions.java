package com.example.cuttonsimulator;

import androidx.annotation.NonNull;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class HelpFunctions
{
    //uCutton.setText("Cutton: " + HelpFunctions.shortenNumber(Float.valueOf(user.getData().getCutton())));
    public static String shortenNumber(float num)
    {
        String[] Letter = {"k", "m", "b", "t", "qd", "qt", "sx", "se", "oc", "no", "de", "ud", "dd"};
        int index = -1;
        String strNum = String.valueOf(num);

        if(num < 1000)
        {
            return strNum;
        }
        while(strNum.length() > 3)
        {
            num = num / 1000;
            strNum = String.valueOf((int)Math.round(num));
            index++;
        }
        if(index > 12)
        {
            strNum = "MAX";
        }
        else
        {
            strNum = String.format("%.1f", num) + Letter[index];
        }
        return strNum;
    }
}
