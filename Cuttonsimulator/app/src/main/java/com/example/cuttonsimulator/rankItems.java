package com.example.cuttonsimulator;

public class rankItems
{
    private int rank;
    private String name;
    private int amount;

    public rankItems(){}

    public rankItems(int rank, String name, int amount)
    {
        this.rank = rank;
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public int getRank() {
        return rank;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
