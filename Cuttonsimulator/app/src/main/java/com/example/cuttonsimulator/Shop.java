package com.example.cuttonsimulator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Shop extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    //layouts.
    RelativeLayout llShop;
    LinearLayout llData;
    LinearLayout llBtns;
    //Strings.
    private String name;

    //Database.
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    //TextViews.
    private TextView uName;
    private TextView uCutton;
    private TextView uLevel;
    private TextView uEventCoins;
    private List<Integer> skinsList;
    private List<Integer> charactersList;
    private List<Integer> bodyList;
    private equip e;
    private equip eC;
    private equip eB;

    //buttons.
    Button Shop;
    Button Inventory;
    Button Game;
    Button ranks;
    Button quests;
    Button skins;
    Button carts;
    Button potions;
    Button upgrades;
    Button cottonBtn;
    Button characters;
    String[] splited;
    Button returnBtn;
    int value = 0;

    //listViews
    //carts
    ListView ls;
    ArrayList<Skins> lst;
    SkinsAdapter adapter;
    //characters
    ListView lsC;
    ArrayList<Skins> lstC;
    SkinsAdapter adapterC;
    //body skins.
    ListView lsB;
    ArrayList<Skins> lstB;
    SkinsAdapter adapterB;
    //upgrades
    ListView lsU;
    ArrayList<upgrades> lstU;
    upgradesManager UM;
    UpgradesAdapter adapterU;


    //skins.
    Skins s1;
    Skins s2;
    Skins s3;
    Skins s4;
    Skins s5;
    Skins s6;

    //characters.
    Skins c1;
    Skins c2;
    Skins c3;
    Skins c4;
    Skins c5;

    //bodies.
    Skins b1;
    Skins b2;
    Skins b3;

    //winFlag.
    int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        //Layouts
        //data layout.
        llData = (LinearLayout)findViewById(R.id.dataLLShop);
        ViewGroup.LayoutParams lp1 = llData.getLayoutParams();
        lp1.height = 50;
        llData.setLayoutParams(lp1);
        //game layout.
        llShop = (RelativeLayout)findViewById(R.id.sizeLShop);
        ViewGroup.LayoutParams lp = llShop.getLayoutParams();
        lp.height = height - 230;
        llShop.setLayoutParams(lp);
        //btns layout
        llBtns = (LinearLayout)findViewById(R.id.llbtnsShop);
        ViewGroup.LayoutParams lp2 = llBtns.getLayoutParams();
        lp2.height = 180;
        llBtns.setLayoutParams(lp2);

        //set the buttons.
        returnBtn = findViewById(R.id.returnBtn);
        returnBtn.setOnClickListener(this);
        skins = findViewById(R.id.skinsBtn);
        skins.setOnClickListener(this);
        upgrades = findViewById(R.id.upgradesBtn);
        upgrades.setOnClickListener(this);
        potions = findViewById(R.id.potionsBtn);
        potions.setOnClickListener(this);
        carts = findViewById(R.id.cartsBtn);
        carts.setOnClickListener(this);
        cottonBtn = findViewById(R.id.cottonBtn);
        cottonBtn.setOnClickListener(this);
        characters = findViewById(R.id.charactersBtn);
        characters.setOnClickListener(this);
        Shop = findViewById(R.id.shopBtnShop);
        Shop.setWidth(displayMetrics.widthPixels / 6);
        Shop.setOnClickListener(this);
        Inventory = findViewById(R.id.inventoryBtnShop);
        Inventory.setWidth(displayMetrics.widthPixels / 6);
        Inventory.setOnClickListener(this);
        Game = findViewById(R.id.gameBtnShop);
        Game.setWidth((displayMetrics.widthPixels / 6) * 2);
        Game.setOnClickListener(this);
        ranks = findViewById(R.id.ranksBtnShop);
        ranks.setWidth(displayMetrics.widthPixels / 6);
        ranks.setOnClickListener(this);
        quests = findViewById(R.id.questsBtnShop);
        quests.setWidth(displayMetrics.widthPixels / 6);
        quests.setOnClickListener(this);
        //set all the editTexts.
        uName = findViewById(R.id.NameShop);
        uName.setWidth(displayMetrics.widthPixels / 4);
        uCutton = findViewById(R.id.cuttonShop);
        uCutton.setWidth(displayMetrics.widthPixels / 4);
        uLevel = findViewById(R.id.LevelShop);
        uLevel.setWidth(displayMetrics.widthPixels / 4);
        uEventCoins = findViewById(R.id.eventcoinsShop);
        uEventCoins.setWidth(displayMetrics.widthPixels / 4);
        //views. (adapters & listViews):
        ls = new ListView(this);
        ls.setOnItemClickListener(this);
        lsC = new ListView(this);
        lsC.setOnItemClickListener(this);
        lsB = new ListView(this);
        lsB.setOnItemClickListener(this);
        lsU = new ListView(this);
        lsU.setOnItemClickListener(this);
        //according to the username set all the needed data for the user (like money, cutton, items and stuff).
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        Query q=myRef.child("Users");
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                SharedPreferences sp1=getSharedPreferences("Login",0 );

                if(sp1.getString("Unm", null) != null)
                    name = sp1.getString("Unm", null);
                for(DataSnapshot snp : snapshot.getChildren())
                {
                    Users user = snp.getValue(Users.class);
                    if(user.getName().equals(name))
                    {
                        uName.setText("Name: " + user.getName());
                        uLevel.setText("Level: " + String.valueOf(user.getData().getLevel()));
                        uCutton.setText("Cutton: " + String.valueOf(user.getData().getCutton()));
                        uEventCoins.setText("EventCoins: " + String.valueOf(user.getData().getEventCoins()));
                        skinsList = user.getData().getSkins();
                        charactersList = user.getData().getCharacters();
                        bodyList = user.getData().getBody();
                        e = user.getData().getEquip();
                        eC = user.getData().getEquipC();
                        eB = user.getData().getEquipB();
                        UM = user.getData().getUpgradesManager();
                        //upgrades list.
                        //add here more skins if u want.
                        lstU = new ArrayList<upgrades>();
                        for(int i = 0; i < UM.getUp().size(); i++)
                        {
                            lstU.add(UM.getUp().get(i));
                        }
                        adapterU = new UpgradesAdapter(Shop.getContext(),lstU);
                        lsU.setAdapter(adapterU);

                        //characters skins.
                        if(charactersList.contains(R.drawable.character1))
                        {
                            c1 = new Skins(0, "black man (default)", 0, R.drawable.character1, true, eC.getId());
                        }
                        else
                        {
                            c1 = new Skins(0, "black man (default)", 0, R.drawable.character1, false, eC.getId());
                        }
                        if(charactersList.contains(R.drawable.barda))
                        {
                            c2 = new Skins(1, "barda", 100, R.drawable.barda, true, eC.getId());
                        }
                        else
                        {
                            c2 = new Skins(1, "barda", 100, R.drawable.barda, false, eC.getId());
                        }
                        if(charactersList.contains(R.drawable.atiya))
                        {
                            c3 = new Skins(2, "atiya", 500, R.drawable.atiya, true, eC.getId());
                        }
                        else
                        {
                            c3 = new Skins(2, "atiya", 500, R.drawable.atiya, false, eC.getId());
                        }
                        if(charactersList.contains(R.drawable.azran))
                        {
                            c4 = new Skins(3, "azran", 1000, R.drawable.azran, true, eC.getId());
                        }
                        else
                        {
                            c4 = new Skins(3, "azran", 1000, R.drawable.azran, false, eC.getId());
                        }
                        if(charactersList.contains(R.drawable.harel))
                        {
                            c5 = new Skins(4, "harel", 1500, R.drawable.harel, true, eC.getId());
                        }
                        else
                        {
                            c5 = new Skins(4, "harel", 1500, R.drawable.harel, false, eC.getId());
                        }

                        //body skins
                        if(bodyList.contains(R.drawable.stick))
                        {
                            b1 = new Skins(0, "Stick (default)", 0, R.drawable.stick, true, eB.getId());
                        }
                        else
                        {
                            b1 = new Skins(0, "Stick (default)", 0, R.drawable.stick, false, eB.getId());
                        }
                        if(bodyList.contains(R.drawable.suitbody))
                        {
                            b2 = new Skins(1, "principal", 250, R.drawable.suitbody, true, eB.getId());
                        }
                        else
                        {
                            b2 = new Skins(1, "principal", 250, R.drawable.suitbody, false, eB.getId());
                        }
                        if(bodyList.contains(R.drawable.suitbody2))
                        {
                            b3 = new Skins(2, "business man", 500, R.drawable.suitbody2, true, eB.getId());
                        }
                        else
                        {
                            b3 = new Skins(2, "business man", 500, R.drawable.suitbody2, false, eB.getId());
                        }

                        //carts skins
                        if(skinsList.contains(R.drawable.cart))
                        {
                            s1 = new Skins(0, "red cart (default)", 0, R.drawable.cart, true, e.getId());
                        }
                        else
                        {
                            s1 = new Skins(0, "red cart (default)", 0, R.drawable.cartblack, false, e.getId());
                        }
                        if(skinsList.contains(R.drawable.cartblack))
                        {
                            s2 = new Skins(1, "black cart", 100, R.drawable.cartblack, true, e.getId());
                        }
                        else
                        {
                            s2 = new Skins(1, "black cart", 100, R.drawable.cartblack, false, e.getId());
                        }
                        if(skinsList.contains(R.drawable.cartgreen))
                        {
                            s3 = new Skins(2, "green cart", 250, R.drawable.cartgreen, true, e.getId());
                        }
                        else
                        {
                            s3 = new Skins(2, "green cart", 250, R.drawable.cartgreen, false, e.getId());
                        }
                        if(skinsList.contains(R.drawable.cartpink))
                        {
                            s4 = new Skins(3, "pink cart", 500, R.drawable.cartpink, true, e.getId());
                        }
                        else
                        {
                            s4 = new Skins(3, "pink cart", 500, R.drawable.cartpink, false, e.getId());
                        }
                        if(skinsList.contains(R.drawable.cartpurple))
                        {
                            s5 = new Skins(4, "purple cart", 750, R.drawable.cartpurple, true, e.getId());
                        }
                        else
                        {
                            s5 = new Skins(4, "purple cart", 750, R.drawable.cartpurple, false, e.getId());
                        }
                        if(skinsList.contains(R.drawable.cartyellow))
                        {
                            s6 = new Skins(5, "yellow cart", 1000, R.drawable.cartyellow, true, e.getId());
                        }
                        else
                        {
                            s6 = new Skins(5, "yellow cart", 1000, R.drawable.cartyellow, false, e.getId());
                        }
                        //add here more skins if u want.
                        lst = new ArrayList<Skins>();
                        lst.add(s1);
                        lst.add(s2);
                        lst.add(s3);
                        lst.add(s4);
                        lst.add(s5);
                        lst.add(s6);
                        adapter = new SkinsAdapter(Shop.getContext(),lst);
                        ls.setAdapter(adapter);
                        //set characters skins.
                        lstC = new ArrayList<Skins>();
                        lstC.add(c1);
                        lstC.add(c2);
                        lstC.add(c3);
                        lstC.add(c4);
                        lstC.add(c5);
                        adapterC = new SkinsAdapter(Shop.getContext(),lstC);
                        lsC.setAdapter(adapterC);
                        //set body skins
                        lstB = new ArrayList<Skins>();
                        lstB.add(b1);
                        lstB.add(b2);
                        lstB.add(b3);
                        adapterB = new SkinsAdapter(Shop.getContext(),lstB);
                        lsB.setAdapter(adapterB);
                        //add here more skins if u want.
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    @Override
    public void onClick(View view)
    {
        if(view == Game)
        {
            Intent intent=new Intent(com.example.cuttonsimulator.Shop.this, GameCutton.class);
            startActivity(intent);
        }
        else if(view == Inventory)
        {
            //move to the inventory window
            Intent intent=new Intent(com.example.cuttonsimulator.Shop.this, Inventory.class);
            startActivity(intent);
        }
        else if(view == ranks)
        {
            Intent intent=new Intent(com.example.cuttonsimulator.Shop.this, ranking.class);
            startActivity(intent);
        }
        else if(view == quests)
        {
            Intent intent=new Intent(com.example.cuttonsimulator.Shop.this, quests.class);
            startActivity(intent);
        }
        else if (view == returnBtn)
        {
            llShop.removeAllViews();
            //hide return button again.
            llShop.addView(returnBtn);
            returnBtn.setVisibility(View.INVISIBLE);
            //return shop buttons.
            llShop.addView(skins);
            llShop.addView(upgrades);
            llShop.addView(cottonBtn);
            llShop.addView(carts);
            llShop.addView(characters);
            llShop.addView(potions);
        }
        else if(view == skins)
        {
            flag = 1;
            //remove the views
            llShop.removeView(upgrades);
            llShop.removeView(skins);
            llShop.removeView(cottonBtn);
            llShop.removeView(carts);
            llShop.removeView(characters);
            llShop.removeView(potions);
            //make the return button visible:
            returnBtn.setVisibility(View.VISIBLE);
            //show the skins list view
            llShop.addView(lsB);
        }
        else if(view == upgrades)
        {
            flag = 2;
            //remove the views
            llShop.removeView(upgrades);
            llShop.removeView(skins);
            llShop.removeView(cottonBtn);
            llShop.removeView(carts);
            llShop.removeView(characters);
            llShop.removeView(potions);
            //make the return button visible:
            returnBtn.setVisibility(View.VISIBLE);
            //show the skins list view
            llShop.addView(lsU);
        }
        else if (view == cottonBtn)
        {
            flag = 3;
            //display cotton list instead of the sizeLshop layout.
        }
        else if(view == carts)
        {
            flag = 4;
            //remove the views
            llShop.removeView(upgrades);
            llShop.removeView(skins);
            llShop.removeView(cottonBtn);
            llShop.removeView(carts);
            llShop.removeView(characters);
            llShop.removeView(potions);
            //make the return button visible:
            returnBtn.setVisibility(View.VISIBLE);
            //show the skins list view
            llShop.addView(ls);
        }
        else if(view == characters)
        {
            flag = 5;
            //remove the views
            llShop.removeView(upgrades);
            llShop.removeView(skins);
            llShop.removeView(cottonBtn);
            llShop.removeView(carts);
            llShop.removeView(characters);
            llShop.removeView(potions);
            //make the return button visible:
            returnBtn.setVisibility(View.VISIBLE);
            //show the skins list view
            llShop.addView(lsC);
        }
        else if(view == potions)
        {
            flag = 6;
            //display potions list instead of the sizeLshop layout.
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        if(flag == 1)
        {
            Skins skinClicked = (Skins)adapterView.getItemAtPosition(i);
            boolean isExist = false;
            //now check if the item is already owned. (get the db)
            for(int j = 0; j < bodyList.size(); j++)
            {
                if (String.valueOf((Integer)skinClicked.getImg()).equals(String.valueOf((Integer)bodyList.get(j))))
                {
                    isExist = true;
                }
            }
            if(isExist == false)
            {
                //check if has enough cottons to buy the skin:
                splited = uCutton.getText().toString().split("\\s+");
                value =  Integer.valueOf(splited[1]);
                if(value >= skinClicked.getPrice())
                {
                    //dec the skin price from our currency.
                    value -= skinClicked.getPrice();
                    uCutton.setText("Cotton: " + String.valueOf(value));
                    //add the skin.
                    bodyList.add(skinClicked.getImg());
                    //equip the skin.
                    equip lastEquipped = new equip(bodyList.indexOf(skinClicked.getImg()), skinClicked.getId());
                    //update the database.
                    HashMap Usr = new HashMap();
                    myRef.child("Users").updateChildren(Usr).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task)
                        {
                            myRef.child("Users").child(name).child("data").child("cutton").setValue(value);
                            myRef.child("Users").child(name).child("data").child("body").setValue(bodyList);
                            myRef.child("Users").child(name).child("data").child("equipB").setValue(lastEquipped);
                        }
                    });
                    for(int j = 0; j < lstB.size(); j++)
                    {
                        lstB.get(j).setLastId(skinClicked.getId());
                        if(String.valueOf((Integer)lstB.get(j).getImg()).equals(String.valueOf((Integer)skinClicked.getImg())))
                        {
                            lstB.get(j).setOwned(true);
                        }
                    }
                    adapterB = new SkinsAdapter(Shop.getContext(),lstB);
                    lsB.setAdapter(adapterB);
                    Toast.makeText(this, "Bought!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(this, "U dont have enough money", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                if(skinClicked.getLastId() == skinClicked.getId())
                {
                    Toast.makeText(this, "already Equipped", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    equip lastEquipped = new equip(bodyList.indexOf(skinClicked.getImg()), skinClicked.getId());
                    HashMap Usr = new HashMap();
                    myRef.child("Users").updateChildren(Usr).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task)
                        {
                            myRef.child("Users").child(name).child("data").child("equipB").setValue(lastEquipped);
                        }
                    });
                    for(int j = 0; j < lstB.size(); j++)
                    {
                        lstB.get(j).setLastId(skinClicked.getId());
                        if(String.valueOf((Integer)lstB.get(j).getImg()).equals(String.valueOf((Integer)skinClicked.getImg())))
                        {
                            lstB.get(j).setOwned(true);

                        }
                    }
                    adapterB = new SkinsAdapter(Shop.getContext(),lstB);
                    lsB.setAdapter(adapterB);
                    Toast.makeText(this, "item equipped!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if (flag == 2)
        {
            upgrades upgradeClicked = (upgrades)adapterView.getItemAtPosition(i);
            //get the uprade clicked.
            int pos = -1;
            //now check if the item is already owned. (get the db)
            for(int j = 0; j < lstU.size(); j++)
            {
                if (upgradeClicked.getName().equals(UM.getUp().get(j).getName()))
                {
                    pos = j;
                }
            }
            //check if has enough cottons to buy the skin:
            splited = uCutton.getText().toString().split("\\s+");
            value =  Integer.valueOf(splited[1]);
            if(value >= upgradeClicked.getPrice())
            {
                //dec the skin price from our currency.
                value -= upgradeClicked.getPrice();
                uCutton.setText("Cotton: " + String.valueOf(value));
                //add +1 rank to the item
                UM.getUp().get(pos).setRank(UM.getUp().get(pos).getRank() + 1);
                //update the price of the upgrade.
                UM.getUp().get(pos).setPrice((UM.getUp().get(pos).getPrice() * UM.getUp().get(pos).getRank()) * 2);
                //upgrade the upgrade.
                UM.getUp().get(pos).setAmount(UM.getUp().get(pos).getAmount() + 1);
                //update the database.
                HashMap Usr = new HashMap();
                myRef.child("Users").updateChildren(Usr).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task)
                    {
                        myRef.child("Users").child(name).child("data").child("cutton").setValue(value);
                        myRef.child("Users").child(name).child("data").child("upgradesManager").setValue(UM);
                    }
                });
                lstU.clear();
                for(int j = 0; j < UM.getUp().size(); j++)
                {
                    lstU.add(UM.getUp().get(j));
                }
                adapterU = new UpgradesAdapter(Shop.getContext(),lstU);
                lsU.setAdapter(adapterU);
                Toast.makeText(this, "Upgraded!", Toast.LENGTH_SHORT).show();

            }
            else
            {
                Toast.makeText(this, "You don't have enough cotton!", Toast.LENGTH_SHORT).show();
            }
        }
        else if (flag == 3)
        {

        }
        else if (flag == 4)
        {
            Skins skinClicked = (Skins)adapterView.getItemAtPosition(i);
            boolean isExist = false;
            //now check if the item is already owned. (get the db)
            for(int j = 0; j < skinsList.size(); j++)
            {
                if (String.valueOf((Integer)skinClicked.getImg()).equals(String.valueOf((Integer)skinsList.get(j))))
                {
                    isExist = true;
                }
            }
            if(isExist == false)
            {
                //check if has enough cottons to buy the skin:
                splited = uCutton.getText().toString().split("\\s+");
                value =  Integer.valueOf(splited[1]);
                if(value >= skinClicked.getPrice())
                {
                    //dec the skin price from our currency.
                    value -= skinClicked.getPrice();
                    uCutton.setText("Cotton: " + String.valueOf(value));
                    //add the skin.
                    skinsList.add(skinClicked.getImg());
                    //equip the skin.
                    equip lastEquipped = new equip(skinsList.indexOf(skinClicked.getImg()), skinClicked.getId());
                    //update the database.
                    HashMap Usr = new HashMap();
                    myRef.child("Users").updateChildren(Usr).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task)
                        {
                            myRef.child("Users").child(name).child("data").child("cutton").setValue(value);
                            myRef.child("Users").child(name).child("data").child("skins").setValue(skinsList);
                            myRef.child("Users").child(name).child("data").child("equip").setValue(lastEquipped);
                        }
                    });
                    for(int j = 0; j < lst.size(); j++)
                    {
                        lst.get(j).setLastId(skinClicked.getId());
                        if(String.valueOf((Integer)lst.get(j).getImg()).equals(String.valueOf((Integer)skinClicked.getImg())))
                        {
                            lst.get(j).setOwned(true);
                        }
                    }
                    adapter = new SkinsAdapter(Shop.getContext(),lst);
                    ls.setAdapter(adapter);
                    Toast.makeText(this, "Bought!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(this, "U dont have enough money", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                if(skinClicked.getLastId() == skinClicked.getId())
                {
                    Toast.makeText(this, "already Equipped", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    equip lastEquipped = new equip(skinsList.indexOf(skinClicked.getImg()), skinClicked.getId());
                    HashMap Usr = new HashMap();
                    myRef.child("Users").updateChildren(Usr).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task)
                        {
                            myRef.child("Users").child(name).child("data").child("equip").setValue(lastEquipped);
                        }
                    });
                    for(int j = 0; j < lst.size(); j++)
                    {
                        lst.get(j).setLastId(skinClicked.getId());
                        if(String.valueOf((Integer)lst.get(j).getImg()).equals(String.valueOf((Integer)skinClicked.getImg())))
                        {
                            lst.get(j).setOwned(true);

                        }
                    }
                    adapter = new SkinsAdapter(Shop.getContext(),lst);
                    ls.setAdapter(adapter);
                    Toast.makeText(this, "item equipped!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if (flag == 5)
        {
            Skins skinClicked = (Skins)adapterView.getItemAtPosition(i);
            boolean isExist = false;
            //now check if the item is already owned. (get the db)
            for(int j = 0; j < charactersList.size(); j++)
            {
                if (String.valueOf((Integer)skinClicked.getImg()).equals(String.valueOf((Integer)charactersList.get(j))))
                {
                    isExist = true;
                }
            }
            if(isExist == false)
            {
                //check if has enough cottons to buy the skin:
                splited = uCutton.getText().toString().split("\\s+");
                value =  Integer.valueOf(splited[1]);
                if(value >= skinClicked.getPrice())
                {
                    //dec the skin price from our currency.
                    value -= skinClicked.getPrice();
                    uCutton.setText("Cotton: " + String.valueOf(value));
                    //add the skin.
                    charactersList.add(skinClicked.getImg());
                    //equip the skin.
                    equip lastEquipped = new equip(charactersList.indexOf(skinClicked.getImg()), skinClicked.getId());
                    //update the database.
                    HashMap Usr = new HashMap();
                    myRef.child("Users").updateChildren(Usr).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task)
                        {
                            myRef.child("Users").child(name).child("data").child("cutton").setValue(value);
                            myRef.child("Users").child(name).child("data").child("characters").setValue(charactersList);
                            myRef.child("Users").child(name).child("data").child("equipC").setValue(lastEquipped);
                        }
                    });
                    for(int j = 0; j < lstC.size(); j++)
                    {
                        lstC.get(j).setLastId(skinClicked.getId());
                        if(String.valueOf((Integer)lstC.get(j).getImg()).equals(String.valueOf((Integer)skinClicked.getImg())))
                        {
                            lstC.get(j).setOwned(true);
                        }
                    }
                    adapterC = new SkinsAdapter(Shop.getContext(),lstC);
                    lsC.setAdapter(adapterC);
                    Toast.makeText(this, "Bought!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(this, "U dont have enough money", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                if(skinClicked.getLastId() == skinClicked.getId())
                {
                    Toast.makeText(this, "already Equipped", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    equip lastEquipped = new equip(charactersList.indexOf(skinClicked.getImg()), skinClicked.getId());
                    HashMap Usr = new HashMap();
                    myRef.child("Users").updateChildren(Usr).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task)
                        {
                            myRef.child("Users").child(name).child("data").child("equipC").setValue(lastEquipped);
                        }
                    });
                    for(int j = 0; j < lstC.size(); j++)
                    {
                        lstC.get(j).setLastId(skinClicked.getId());
                        if(String.valueOf((Integer)lstC.get(j).getImg()).equals(String.valueOf((Integer)skinClicked.getImg())))
                        {
                            lstC.get(j).setOwned(true);

                        }
                    }
                    adapterC = new SkinsAdapter(Shop.getContext(),lstC);
                    lsC.setAdapter(adapterC);
                    Toast.makeText(this, "item equipped!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if(flag == 6)
        {

        }
    }
}