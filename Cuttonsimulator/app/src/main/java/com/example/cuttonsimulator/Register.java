package com.example.cuttonsimulator;

import com.google.firebase.database.Exclude;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.MutableBoolean;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Register extends AppCompatActivity implements View.OnClickListener {
    EditText user;
    EditText pass;
    Button Register;
    Button Login;
    public static boolean isExist;
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);
        Register = findViewById(R.id.register);
        Register.setOnClickListener(this);
        Login = findViewById(R.id.login);
        Login.setOnClickListener(this);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        VideoView videoview = (VideoView) findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.intro);
        videoview.setVideoURI(uri);
        videoview.start();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                user.setVisibility(View.VISIBLE);
                pass.setVisibility(View.VISIBLE);
                Register.setVisibility(View.VISIBLE);
                Login.setVisibility(View.VISIBLE);
            }
        }, 4100);

    }


    @Override
    public void onClick(View view) {
        if(view == Register)
        {
            isExist = false;
            if(user.getText().toString().length() > 0 && pass.getText().toString().length() > 0)
            {
                //check if already exist.
                Query q=myRef.child("Users");
                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for(DataSnapshot snp : snapshot.getChildren())
                        {
                            Users u1 = snp.getValue(Users.class);
                            if(u1.getName().equals(user.getText().toString()))
                            {
                                //the prob it doesn't save outside this func.
                                isExist = true;
                                Toast.makeText(Register.this, "This user already exist!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        if(!isExist)
                        {
                            SharedPreferences sp = getSharedPreferences("Login",0 );
                            SharedPreferences.Editor Ed=sp.edit();
                            Ed.putString("Unm",user.getText().toString());
                            Ed.putString("Psw", pass.getText().toString());
                            Ed.commit();
                            List<Integer> newSkins = new ArrayList<Integer>();
                            List<Integer> newCharacters = new ArrayList<Integer>();
                            List<Integer> newBody = new ArrayList<Integer>();
                            newSkins.add(R.drawable.cart);
                            newCharacters.add(R.drawable.character1);
                            newBody.add(R.drawable.stick);
                            equip e = new equip(0, 0);
                            equip eC = new equip(0, 0);
                            equip eB = new equip(0, 0);
                            List<Integer> newUpgrades = new ArrayList<Integer>();
                            upgrades up1 = new upgrades("Cotton", 1, 10, 1);
                            upgrades up2 = new upgrades("Speed", 8, 10, 1);
                            List<upgrades> ul = new ArrayList<>();
                            ul.add(up1);
                            ul.add(up2);
                            upgradesManager upgradesManager = new upgradesManager(ul);
                            UserData ud = new UserData(0,0,0, newSkins ,e, 0, newCharacters, eC, upgradesManager, newBody, eB);
                            Users u1 = new Users(user.getText().toString(), pass.getText().toString(), ud);
                            myRef.child("Users").child(u1.getName()).setValue(u1);
                            Intent intent=new Intent(com.example.cuttonsimulator.Register.this, GameCutton.class);
                            startActivity(intent);
                            Toast.makeText(Register.this, "ADDED!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
            else
            {
                Toast.makeText(Register.this, "Username / password cant be empty!", Toast.LENGTH_SHORT).show();
            }
        }
        else if(view == Login)
        {
            Intent intent=new Intent(this, Login.class);
            startActivity(intent);
        }
    }
}