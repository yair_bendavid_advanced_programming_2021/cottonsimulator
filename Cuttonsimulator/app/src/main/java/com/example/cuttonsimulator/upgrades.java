package com.example.cuttonsimulator;

public class upgrades
{
    private String Name;
    private int amount;
    private int Price;
    private int rank;

    public upgrades(){}

    public upgrades(String Name, int amount, int price, int rank)
    {
        this.Name = Name;
        this.amount = amount;
        this.Price = price;
        this.rank = rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getRank() {
        return rank;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public String getName() {
        return Name;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

}
