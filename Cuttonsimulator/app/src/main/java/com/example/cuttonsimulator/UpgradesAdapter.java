package com.example.cuttonsimulator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;


public class UpgradesAdapter extends ArrayAdapter<upgrades>
{
    private  Context context;
    private ArrayList<upgrades> list;
    public UpgradesAdapter(@NonNull Context context, ArrayList<upgrades> list) {
        super(context, R.layout.skins_layout,list);
        this.context = context;
        this.list=list;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = ((AppCompatActivity) context).getLayoutInflater();
        // create view object from custom layout xml
        View view = layoutInflater.inflate(R.layout.upgrades_layout, parent, false);
        // get a reference to a sport object
        upgrades f = this.list.get(position);
        // references to inflated view elements
        // custom layout views
        TextView Name = view.findViewById(R.id.name);
        TextView amount = view.findViewById(R.id.amount);
        TextView Price = view.findViewById(R.id.price);
        TextView rank = view.findViewById(R.id.rank);
        // get the element name and image resource id
        Name.setText(f.getName());
        amount.setText(f.getName() + ": " + String.valueOf(f.getAmount()));
        Price.setText(String.valueOf(f.getPrice()));
        rank.setText("Rank: " + String.valueOf(f.getRank()));

        return view;
    }
}
